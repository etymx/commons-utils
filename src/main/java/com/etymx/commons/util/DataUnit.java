package com.etymx.commons.util;

/**
 * @author LiangYongjie
 * @date 2020-07-12
 */
public enum DataUnit {
    B,
    KB,
    MB,
    GB,
    TB,
    PB,
    EB,
    ZB,
    YB,
    BB
}
