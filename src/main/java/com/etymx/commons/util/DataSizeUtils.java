package com.etymx.commons.util;

import java.util.Arrays;
import java.util.List;

/**
 * 数据大小转换工具类
 *
 * @author EtyMx
 * @date 2020-07-12
 */
public class DataSizeUtils {

    private static final List<DataUnit> UNITS = Arrays.asList(DataUnit.values());

    private static final int RADIX = 1024;
    private static final double LOG_RADIX = Math.log(1024);

    /**
     * 将数据大小的单位转换为目标单位
     *
     * @param value  数据大小
     * @param src    原始单位
     * @param target 目标单位
     * @return 转为目标单位后的数据
     * @see Data
     */
    public static Data conversion(double value, DataUnit src, DataUnit target) {
        if (value <= 0 || src == target) {
            return new Data(value, src);
        }

        int srcIndex = UNITS.indexOf(src);
        int targetIndex = UNITS.indexOf(target);
        return new Data(value * Math.pow(RADIX, srcIndex - targetIndex), target);
    }

    /**
     * 自适应转换数据大小
     *
     * @param value 数据大小
     * @param src   当前单位
     * @return 自适应转换后的数据大小
     * @see Data
     */
    public static Data autoConversion(double value, DataUnit src) {
        if (value <= 0) {
            return new Data(value, src);
        }

        int srcIndex = UNITS.indexOf(src);
        int targetIndex = (int) Math.floor(Math.log(value) / LOG_RADIX) + srcIndex;
        if (targetIndex < 0) {
            return new Data(value, src);
        }
        if (targetIndex >= UNITS.size()) {
            targetIndex = UNITS.size() - 1;
        }

        return conversion(value, src, UNITS.get(targetIndex));
    }

}
