package com.etymx.commons.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 数字处理工具类
 *
 * @author LiangYongjie
 * @date 2020-07-12
 */
public class NumberUtils {

    public static BigDecimal HUNDRED = new BigDecimal("100");

    public static double setFraction(double d, int scale, RoundingMode roundingMode) {
        BigDecimal decimal = BigDecimal.valueOf(d);
        return decimal.setScale(scale, roundingMode).doubleValue();
    }

    public static double setFraction(double d, int scale) {
        return setFraction(d, scale, RoundingMode.DOWN);
    }

    public static float setFraction(float f, int scale, RoundingMode roundingMode) {
        BigDecimal decimal = BigDecimal.valueOf(f);
        return decimal.setScale(scale, roundingMode).floatValue();
    }

    public static float setFraction(float f, int scale) {
        return setFraction(f, scale, RoundingMode.DOWN);
    }

}
