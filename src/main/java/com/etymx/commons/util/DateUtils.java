package com.etymx.commons.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期工具类
 *
 * @author LiangYongjie
 * @date 2020-07-11
 */
public class DateUtils {

    public static final String DEFAULT_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static String format(String pattern, Date date) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    public static String format(String pattern, long timestamp) {
        return format(pattern, new Date(timestamp));
    }

    public static String format(Date date) {
        return format(DEFAULT_PATTERN, date);
    }

    public static String format(long timestamp) {
        return format(DEFAULT_PATTERN, timestamp);
    }

    public static Date parseToDate(String pattern, String date) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            return format.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static long parseToTimestamp(String pattern, String date) {
        Date d = parseToDate(pattern, date);
        return d != null ? d.getTime() : 0;
    }

    /**
     * 判断两个日期是否为同一天
     *
     * @param date1 日期1
     * @param date2 日期2
     * @return 返回布尔值，日期1和日期2是否为同一天
     */
    public static boolean isSameDay(Date date1, Date date2) {
        return endOfDay(date1).equals(endOfDay(date2));
    }

    /**
     * 获取日期的开始时间
     *
     * @param calendar 日期
     * @return 该日期的开始时间
     */
    public static Calendar startOfDay(Calendar calendar) {
        return clearTime(calendar);
    }

    /**
     * 获取日期的开始时间
     *
     * @param date 日期
     * @return 该日期的开始时间
     */
    public static Date startOfDay(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return startOfDay(c).getTime();
    }

    /**
     * 获取日期的结束时间
     *
     * @param calendar 日期
     * @return 该日期的结束时间
     */
    public static Calendar endOfDay(Calendar calendar) {
        return endTime(calendar);
    }

    /**
     * 获取日期的结束时间
     *
     * @param date 日期
     * @return 该日期的结束时间
     */
    public static Date endOfDay(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return endOfDay(c).getTime();
    }

    /**
     * 获取本周的第一天
     *
     * @param value 一周的第一天是周几，例如 Calendar.MONDAY
     * @return Date对象
     * @see java.util.Calendar
     */
    public static Date firstDayOfWeek(int value) {
        Calendar c = Calendar.getInstance();
        c.setFirstDayOfWeek(value);
        c.set(Calendar.DAY_OF_WEEK, 1);
        return c.getTime();
    }

    /**
     * 获取本周的最后一天
     *
     * @param value 一周的第一天是周几，例如 Calendar.MONDAY
     * @return Date对象
     * @see java.util.Calendar
     */
    public static Date lastDayOfWeek(int value) {
        Calendar c = Calendar.getInstance();
        c.setFirstDayOfWeek(value);
        clearTime(c);
        c.set(Calendar.DAY_OF_WEEK, 7);
        return c.getTime();
    }

    /**
     * 获取本月第一天的开始时间
     *
     * @return 本月第一天的开始Date对象
     */
    public static Date startTimeOfFirstDayOfMonth() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, 1);
        clearTime(c);
        return c.getTime();
    }

    /**
     * 获取本月第一天的结束时间
     *
     * @return 本月第一天的结束Date对象
     */
    public static Date endTimeOfFirstDayOfMonth() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, 1);
        endTime(c);
        return c.getTime();
    }

    /**
     * 获取本月最后一天的开始时间
     *
     * @return 本月最后一天的开始Date对象
     */
    public static Date startTimeOfLastDayOfMonth() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.add(Calendar.MONTH, 1);
        c.add(Calendar.DAY_OF_MONTH, -1);
        clearTime(c);
        return c.getTime();
    }

    /**
     * 获取本月最后一天的结束时间
     *
     * @return 本月最后一天的结束Date对象
     */
    public static Date endTimeOfLastDayOfMonth() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.add(Calendar.MONTH, 1);
        c.add(Calendar.DAY_OF_MONTH, -1);
        endTime(c);
        return c.getTime();
    }

    private static Calendar clearTime(Calendar calendar) {
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    private static Calendar endTime(Calendar calendar) {
        clearTime(calendar);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.MILLISECOND, -1);
        return calendar;
    }

}
