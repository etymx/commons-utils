package com.etymx.commons.util;

/**
 * 数据大小封装类
 *
 * @see DataUnit
 *
 * @author LiangYongjie
 * @date 2020-07-12
 */
public class Data {

    private double value;
    private DataUnit unit;

    public Data(double value, DataUnit unit) {
        this.value = value;
        this.unit = unit;
    }

    public double getValue() {
        return value;
    }

    public DataUnit getUnit() {
        return unit;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void setUnit(DataUnit unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "Data{" +
            "value=" + value +
            ", unit=" + unit +
            '}';
    }
}
